package org.apache.xmlrpc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;

/**
 * Copy of {@link XmlRpcResponseProcessor} except with instances of {@link XmlWriter} replaced with {@link SurrogatePairCapableXmlWriter}
 *
 * @since 1.0
 */
public class SurrogatePairCapableXmlRpcResponseProcessor extends XmlRpcResponseProcessor {
    private static final Logger log = LoggerFactory.getLogger(SurrogatePairCapableXmlRpcResponseProcessor.class);
    private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];

    /**
     * Creates a new instance.
     */
    public SurrogatePairCapableXmlRpcResponseProcessor() {
    }

    /**
     * Process a successful response, and return output in the
     * specified encoding.
     *
     * @param responseParam The response to process.
     * @param encoding      The output encoding.
     * @return byte[] The XML-RPC response.
     */
    public byte[] encodeResponse(Object responseParam, String encoding)
            throws IOException, UnsupportedEncodingException, XmlRpcException {
        long now = 0;
        if (XmlRpc.debug) {
            now = System.currentTimeMillis();
        }

        try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            XmlWriter writer = new SurrogatePairCapableXmlWriter(buffer, encoding);
            writeResponse(responseParam, writer);
            writer.flush();
            return buffer.toByteArray();
        } finally {
            if (XmlRpc.debug) {
                log.debug("Spent {} millis encoding response", (System.currentTimeMillis() - now));
            }
        }
    }

    /**
     * Process an exception, and return output in the specified
     * encoding.
     *
     * @param x        The exception to process;
     * @param encoding The output encoding.
     * @param code     The XML-RPC faultCode.
     * @return byte[] The XML-RPC response.
     */
    public byte[] encodeException(Exception x, String encoding, int code) {
        if (XmlRpc.debug) {
            log.debug("", x);
        }
        // Ensure that if there is anything in the buffer, it
        // is cleared before continuing with the writing of exceptions.
        // It is possible that something is in the buffer
        // if there were an exception during the writeResponse()
        // call above.
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        XmlWriter writer = null;
        try {
            writer = new SurrogatePairCapableXmlWriter(buffer, encoding);
        } catch (UnsupportedEncodingException encx) {
            log.error("XmlRpcServer attempted to use unsupported encoding", encx);
            // NOTE: If we weren't already using the default
            // encoding, we could try it here.
        }

        String message = x.toString();
        // Retrieve XmlRpcException error code(if possible).
        try {
            writeError(code, message, writer);
            writer.flush();
        } catch (Exception e) {
            // Unlikely to occur, as we just sent a struct
            // with an int and a string.
            log.error("Unable to send error response to client", e);
        }

        return (writer != null ? buffer.toByteArray() : EMPTY_BYTE_ARRAY);
    }

    /**
     * Process an exception, and return output in the specified
     * encoding.
     *
     * @param e        The exception to process;
     * @param encoding The output encoding.
     * @return byte[] The XML-RPC response.
     */
    public byte[] encodeException(Exception x, String encoding) {
        return encodeException(x, encoding, (x instanceof XmlRpcException) ? ((XmlRpcException) x).code : 0);
    }

    /**
     * Writes an XML-RPC response to the XML writer.
     */
    void writeResponse(Object param, XmlWriter writer)
            throws XmlRpcException, IOException {
        writer.startElement("methodResponse");
        // if (param == null) param = ""; // workaround for Frontier bug
        writer.startElement("params");
        writer.startElement("param");
        writer.writeObject(param);
        writer.endElement("param");
        writer.endElement("params");
        writer.endElement("methodResponse");
    }

    /**
     * Writes an XML-RPC error response to the XML writer.
     */
    void writeError(int code, String message, XmlWriter writer)
            throws XmlRpcException, IOException {
        // log.error("error: "+message);
        Hashtable h = new Hashtable();
        h.put("faultCode", new Integer(code));
        h.put("faultString", message);
        writer.startElement("methodResponse");
        writer.startElement("fault");
        writer.writeObject(h);
        writer.endElement("fault");
        writer.endElement("methodResponse");
    }


}
