package org.apache.xmlrpc;

/**
 * An implementation of an xml rpc server which is capable of encoding surrogate pairs correctly, instead of splitting
 * them into two invalid entities.
 *
 * @since 1.0
 */
public class SurrogatePairCapableXmlRpcServer extends XmlRpcServer {
    @Override
    protected XmlRpcWorker createWorker() {
        return new SurrogatePairCapableXmlRpcWorker(getHandlerMapping());
    }

    private static class SurrogatePairCapableXmlRpcWorker extends XmlRpcWorker {
        public SurrogatePairCapableXmlRpcWorker(XmlRpcHandlerMapping handlerMapping) {
            super(handlerMapping);
            //override the response processor with one that can encode surrogate pairs properly
            responseProcessor = new SurrogatePairCapableXmlRpcResponseProcessor();
        }
    }
}
