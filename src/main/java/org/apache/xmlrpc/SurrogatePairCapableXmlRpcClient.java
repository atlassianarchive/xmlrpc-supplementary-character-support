package org.apache.xmlrpc;

import java.io.IOException;
import java.net.MalformedURLException;

/**
 * an xml rpc client that overrides the way xml entities are encoded, such that surrogate pairs do not get split, and
 * is instead encoded as one entity.
 *
 * @since 1.0
 */
public class SurrogatePairCapableXmlRpcClient extends XmlRpcClient {
    public SurrogatePairCapableXmlRpcClient(String baseUrl) throws MalformedURLException {
        super(baseUrl);
    }

    @Override
    synchronized XmlRpcClientWorker getWorker(boolean async) throws IOException {
        final XmlRpcClientWorker worker = super.getWorker(async);
        worker.requestProcessor = new SurrogatePairCapableXmlRpcClientRequestProcessor();
        return worker;
    }
}
