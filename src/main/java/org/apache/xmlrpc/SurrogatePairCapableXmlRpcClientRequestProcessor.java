package org.apache.xmlrpc;

import java.io.IOException;
import java.io.OutputStream;

/**
 * A copy of {@link XmlRpcClientRequestProcessor} with the {@link XmlWriter} replaced with a {@link SurrogatePairCapableXmlWriter}.
 *
 * @since 1.0
 */
public class SurrogatePairCapableXmlRpcClientRequestProcessor extends XmlRpcClientRequestProcessor {
    public SurrogatePairCapableXmlRpcClientRequestProcessor() {
        super();
    }

    @Override
    public void encodeRequest(XmlRpcClientRequest request, String encoding, OutputStream out) throws XmlRpcClientException, IOException {
        XmlWriter writer = new SurrogatePairCapableXmlWriter(out, encoding);

        writer.startElement("methodCall");
        writer.startElement("methodName");
        writer.write(request.getMethodName());
        writer.endElement("methodName");
        writer.startElement("params");

        int l = request.getParameterCount();
        for (int i = 0; i < l; i++) {
            writer.startElement("param");
            try {
                writer.writeObject(request.getParameter(i));
            } catch (XmlRpcException e) {
                throw new XmlRpcClientException("Failure writing request", e);
            }
            writer.endElement("param");
        }
        writer.endElement("params");
        writer.endElement("methodCall");
        writer.flush();
    }
}
