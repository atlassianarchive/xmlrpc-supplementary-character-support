package org.apache.xmlrpc;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

/**
 * An xml writer that is capable of encoding surrogate pairs as a single entity, instead of split up into two invalid
 * entities
 *
 * @since 1.0
 */
public class SurrogatePairCapableXmlWriter extends XmlWriter {
    public SurrogatePairCapableXmlWriter(OutputStream out, String encoding) throws UnsupportedEncodingException {
        super(out, encoding);
    }

    /**
     * Writes characters like '\r' (0xd) as "&amp;#13;".
     */
    private void writeCharacterReference(int c) throws IOException {
        if (0x1 <= c && c <= 0x1f){
            write("&amp;#");
        } else {
                write("&#");
        }
        write(String.valueOf(c));
        write(';');
    }

    /**
     * Section 2.2 of the XML spec describes which Unicode code points
     * are valid in XML:
     * <p/>
     * <blockquote><code>#x9 | #xA | #xD | [#x20-#xD7FF] |
     * [#xE000-#xFFFD] | [#x10000-#x10FFFF]</code></blockquote>
     * <p/>
     * Code points outside this set must be entity encoded to be
     * represented in XML.
     *
     * @param c The character to inspect.
     * @return Whether the specified character is valid in XML.
     */
    private static final boolean isValidXMLChar(int c) {
        switch (c) {
            case 0x9:
            case 0xa:  // line feed, '\n'
            case 0xd:  // carriage return, '\r'
                return true;

            default:
                return ((0x20 <= c && c <= 0xd7ff) ||
                        (0xe000 <= c && c <= 0xfffd) ||
                        (0x10000 <= c && c <= 0x10ffff));
        }
    }

    @Override
    protected void chardata(String text) throws XmlRpcException, IOException {
        int l = text.length();
        // ### TODO: Use a buffer rather than going character by
        // ### character to scale better for large text sizes.
        //char[] buf = new char[32];
        for (int i = 0; i < l; i++) {
            char c = text.charAt(i);
            switch (c) {
                case '\t':
                case '\n':
                    write(c);
                    break;
                case '\r':
                    // Avoid normalization of CR to LF.
                    writeCharacterReference(c);
                    break;
                case '<':
                    write(LESS_THAN_ENTITY);
                    break;
                case '>':
                    write(GREATER_THAN_ENTITY);
                    break;
                case '&':
                    write(AMPERSAND_ENTITY);
                    break;
                default:
                    // Though the XML spec requires XML parsers to support
                    // Unicode, not all such code points are valid in XML
                    // documents.  Additionally, previous to 2003-06-30
                    // the XML-RPC spec only allowed ASCII data (in
                    // <string> elements).  For interoperability with
                    // clients rigidly conforming to the pre-2003 version
                    // of the XML-RPC spec, we entity encode characters
                    // outside of the valid range for ASCII, too.
                    if (c > 0x7f || !isValidXMLChar(c)) {
                        if (!Character.isLowSurrogate(c)) {
                            // Replace the code point with a character reference.
                            writeCharacterReference(text.codePointAt(i));
                        }
                        // otherwise, we ignore the character, since its a lowSurrogate.
                    } else {
                        write(c);
                    }
            }
        }
    }
}
