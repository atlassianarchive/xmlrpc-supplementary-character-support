package org.apache.xmlrpc;

import junit.framework.TestCase;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class TestSurrogatePairCapableXmlWriter extends TestCase {
    public TestSurrogatePairCapableXmlWriter() {
        super();
    }

    public TestSurrogatePairCapableXmlWriter(String name) {
        super(name);
    }

    public void testWriteChar() throws Exception {
        assertBytesCorrectlyWritten("simple ascii", "abcd", "abcd");
        assertBytesCorrectlyWritten("surrogate pair", "&#128032;", "\uD83D\uDC20");
        assertBytesCorrectlyWritten("ascii following surrogate pair", "&#128032;abcd", "\uD83D\uDC20abcd");
        assertBytesCorrectlyWritten("ascii sandwiching surrogate pair", "abcd&#128032;abcd", "abcd\uD83D\uDC20abcd");
        assertBytesCorrectlyWritten("ascii prefixing surrogate pair", "abcd&#128032;", "abcd\uD83D\uDC20");
        assertBytesCorrectlyWritten("new line following surrogate pair", "abcd&#128032; \n", "abcd\uD83D\uDC20 \n");
        assertBytesCorrectlyWritten("newline betweentwo  surrogate pairs", "abcd&#128032; \n&#128032;", "abcd\uD83D\uDC20 \n\uD83D\uDc20");
        assertBytesCorrectlyWritten("dangling low surrogate at end", "abcd", "abcd\uDC20");
        assertBytesCorrectlyWritten("dangling low surrogate before proper surrogate pair", "abcd&#128032;", "abcd\uDC20\uD83D\uDC20");

        assertBytesCorrectlyWritten("unicode char greather than 0x7f before surrogate pair", "abcd&#237;&#128032;", "abcd\u00ed\uD83D\uDC20");
        assertBytesCorrectlyWritten("unicode char greather than 0x7f before dangling low surrogate", "abcd&#237;", "abcd\u00ed\uDC20");
        assertBytesCorrectlyWritten("unicode char greather than 0x7f", "abcd&#163;", "abcd\u00A3");
        assertBytesCorrectlyWritten("unicode char greather than 0x7f", "abcd&#164;", "abcd\u00A4");

        assertBytesCorrectlyWritten("chinese characters", "abcd&#38345;&#28023;&#20117;", "abcd\u95c9\u6d77\u4e95");
        assertBytesCorrectlyWritten("chinese characters", "abcd&#38345;&#28023;&#20117;abcd", "abcd\u95c9\u6d77\u4e95abcd");
        //check out these characters at: https://en.wikipedia.org/wiki/Aelig, https://en.wikipedia.org/wiki/%C3%98, https://en.wikipedia.org/wiki/Ring_(diacritic),
        assertBytesCorrectlyWritten("Aelig, Oslash, Aring characters", "abcd&#230;&#248;&#229;", "abcd\u00e6\u00f8\u00e5");
        assertBytesCorrectlyWritten("Aelig, Oslash, Aring characters", "abcd&#230;&#248;&#229;abcd", "abcd\u00e6\u00f8\u00e5abcd");
        assertBytesCorrectlyWritten("Aelig, Oslash, Aring characters", "&#230;&#248;&#229;abcd", "\u00e6\u00f8\u00e5abcd");

        assertBytesCorrectlyWritten("'greater than' symbol", "abcd&gt;", "abcd>");
        assertBytesCorrectlyWritten("non-breaking space", "abcd&#160;", "abcd\u00A0");


        assertBytesCorrectlyWritten("invalid XML character", "&amp;#1;", "\u0001");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#2;", "\u0002");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#3;", "\u0003");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#4;", "\u0004");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#5;", "\u0005");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#6;", "\u0006");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#7;", "\u0007");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#8;", "\u0008");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#11;", "\u000B");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#12;", "\u000C");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#14;", "\u000E");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#15;", "\u000F");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#16;", "\u0010");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#17;", "\u0011");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#18;", "\u0012");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#19;", "\u0013");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#20;", "\u0014");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#21;", "\u0015");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#22;", "\u0016");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#23;", "\u0017");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#24;", "\u0018");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#25;", "\u0019");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#26;", "\u001A");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#27;", "\u001B");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#28;", "\u001C");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#29;", "\u001D");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#30;", "\u001E");
        assertBytesCorrectlyWritten("invalid XML character", "&amp;#31;", "\u001F");
    }

    private void assertBytesCorrectlyWritten(final String description, final String expected, final String text) throws XmlRpcException, IOException {
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        final MySurrogatePairCapableXmlWriter writer = new MySurrogatePairCapableXmlWriter(output);

        writer.chardata(text);
        writer.flush();
        //remove the xml declaration, since the method chardata is not meant to be called directly and so breaks the output
        //order. This method only tests whether the given characters are properly encoded, not whether it outputs correct xml
        assertEquals("Failed: " + description, expected, new String(output.toByteArray()).replace("<?xml version=\"1.0\"?>", ""));
    }

    private static class MySurrogatePairCapableXmlWriter extends SurrogatePairCapableXmlWriter {
        public MySurrogatePairCapableXmlWriter(ByteArrayOutputStream output) throws UnsupportedEncodingException {
            super(output, "UTF8");
        }

        //need to override so it can be called in the test
        public void chardata(String text) throws XmlRpcException, IOException {
            super.chardata(text);
        }
    }
}
